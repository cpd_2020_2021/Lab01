package com.company.office;

public class Printer {

    private boolean busy = false;

    public Printer() {
    }

    public synchronized void printDocument(String document) throws InterruptedException {

        while(busy){
            wait();
            System.out.println("Printer busy");
        }

        busy = true;
        Thread.sleep((long) ((Math.random() * (500 - 800)) + 500));
        System.out.println(document);
        busy = false;
        notifyAll();
    }
}
