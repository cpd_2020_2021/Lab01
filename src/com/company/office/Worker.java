package com.company.office;

import java.util.Arrays;
import java.util.List;

public class Worker extends Thread {
    Printer printer;
    private List<String> documents = Arrays.asList("doc1", "doc2", "doc3", "doc4");

    public Worker(String name,Printer printer) {
        super(name);
        this.printer = printer;
    }

    @Override
    public void run() {
    for(String document : documents) {
        try {
                sleep((long) ((Math.random() * (5000 - 1000)) + 1000));
                printer.printDocument(this.getName() + " " +  document);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
