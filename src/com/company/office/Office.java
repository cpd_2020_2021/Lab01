package com.company.office;

/**
1.5.3
Intr-un birou sunt 8 functionari care din când în când tipăresc la imprimantă documente,
nu toți elaborează documentele în același ritm. Fiindcă au o singură imprimantă în birou, poate tipări
doar o singura persoană la un moment dat. Să se simuleze functionarea biroului.
 **/
public class Office {

    public static void execute(){
        Printer printer = new Printer();

        Worker worker1 = new Worker("Worker 1", printer);
        Worker worker2 = new Worker("Worker 2", printer);
        Worker worker3 = new Worker("Worker 3", printer);
        Worker worker4 = new Worker("Worker 4", printer);
        Worker worker5 = new Worker("Worker 5", printer);
        Worker worker6 = new Worker("Worker 6", printer);
        Worker worker7 = new Worker("Worker 7", printer);
        Worker worker8 = new Worker("Worker 8", printer);

        worker1.start();
        worker2.start();
        worker3.start();
        worker4.start();
        worker5.start();
        worker6.start();
        worker7.start();
        worker8.start();
    }

}
