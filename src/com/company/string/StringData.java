package com.company.string;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class StringData {

    private int leftIndex = 0;
    private int rightIndex;
    private String text;
    private boolean done = false;
    private final static Lock lock =  new ReentrantLock();

    public StringData(String text) {
        this.rightIndex = text.length() -1;
        this.text = text;
    }

    public char getCharacter(boolean ascending){

        if(leftIndex >= rightIndex){
            lock.lock();
            done = true;
            lock.unlock();
            return text.charAt(leftIndex);
        }
        else if(ascending){
            return text.charAt(leftIndex++);
        } else{
            return text.charAt(rightIndex--);
        }
    }

    public boolean isDone() {
        lock.lock();
        lock.unlock();
        return done;
    }
}
