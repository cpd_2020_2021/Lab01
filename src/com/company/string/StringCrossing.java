package com.company.string;

/**
 *1.5.4.
 * Scrieţi un program Java care generează două fire de execuţie pentru parcurgerea unui
 * String de la cele două capete. Folosiţi doi pointeri a căror valoare se incrementează, respectiv se
 * decrementează într-o funcţie din memoria comună (în memoria comună se află String-ul.)
 */
public class StringCrossing {

    public static void execute(){
        StringData stringData = new StringData("Merge!");

        StringProcessor stringProcessor1 = new StringProcessor("Left Thread", true, stringData);
        StringProcessor stringProcessor2 = new StringProcessor("Right Thread", false, stringData);

        stringProcessor1.start();
        stringProcessor2.start();
    }

}
