package com.company.string;

public class StringProcessor extends Thread{

    private boolean ascending;
    private StringData string;

    public StringProcessor(String name, boolean ascending, StringData string) {
        super(name);
        this.ascending = ascending;
        this.string = string;
    }

    @Override
    public void run() {
        while(!string.isDone()){
            char c = string.getCharacter(this.ascending);
            System.out.println("[+] " + this.getName() + " " + c);

            try {
                sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
